const express = require("express");

const mongoose = require("mongoose");

const port =3001;

const app = express();

// [Section] MongoDB connection
	// Syntax:
	// mongoose.connect("mongoDBconnectionString",{ options to avoid error in our connection})

mongoose.connect("mongodb+srv://admin:admin@batch245-refugio.uoibewa.mongodb.net/s35-discussion?retryWrites=true&w=majority",
			{
				// Allows us to avoid any current and future errors while connecting to MONGODB
				useNewUrlParser:true,
				useUnifiedTopology:true
		})
	
	let db = mongoose.connection;

	// error handling in connecting
	db.on("error", console.error.bind(console, "Connection error"));


	// this will be triggered if the connection is succesful
	db.once("open",()=> console.log("We're connected to the cloud database!"))


	// Mongoose Schemas
		// Schemas determine the structure of the documents to be witten in the database
		// Schemas act as the blueprint to our data
		// Syntax:
			// const schemaName = new mongoose.Schema({keyvaluepairs})
		// taskSchema it contains two properties: name & status
		// required is used to specify that a field must not be empty

		// default - is used if a field value is not supplied
		const taskSchema = new mongoose.Schema({
			name:{
				type: String,
				required: [true,"Task name is required!"]
			},
			status:{
				type: String,
				default: "pending"
			}
		})

		const signUpUser = new mongoose.Schema({
			username:{
				type: String,
				required: [true, "Username is required"]
			},
			password:{
				type: String,
				required: [true, "Password is required"]
			}
		})

		// [Section] Models
			// Uses schema to create/instatiate documents/objects that follows our schema structure
			// the variable/object that will be created can be used to run commands with our database
			// Syntax:
				// const variableName = mongoose.model("collectionName",schemaName);

		const Task = mongoose.model("Task", taskSchema)
		const User = mongoose.model("User", signUpUser)



// middlewares
app.use(express.json()) // Allows the app to read json
app.use(express.urlencoded({extended: true})) //it will alliw our app to read data from forms.

// [Section] Routing
	// Create/add new task
		// 1. Check if the task is existing.
				// if task already exist in the database, we will return a mesage "the task is already existing!"
				// if the task doesnt exist in the database, we will add it in the database
	app.post("/tasks", (request, response)=> {
		let input = request.body

			console.log(input.status);
			if(input.status === undefined){
				Task.findOne({name: input.name}, (error, result)=> {
				console.log(result);

				if (result !== null) {
					return response.send("The task is already existing!");
				} else {
					let newTask = new Task({
						name: input.name
					})

					// save() method will save the object
					newTask.save((saveError, savedTask)=> {
						if(saveError){
							return console.log(saveError);
						} else {
							return response.send("New Task created!")
						}
					})
				}
			})
		} else {
			Task.findOne({name: input.name}, (error, result)=> {
				console.log(result);

				if (result !== null) {
					return response.send("The task is already existing!");
				} else {
					let newTask = new Task({
						name: input.name,
						status: input.status
					})

					// save() method will save the object
					newTask.save((saveError, savedTask)=> {
						if(saveError){
							return console.log(saveError);
						} else {
							return response.send("New Task created!")
						}
					})
				}
			})}
	})

	// [Section] Retrieving all the tasks

	app.get("/tasks", (request, response)=>{
		Task.find({}, (error, result)=>{
			if(error){
				console.log(error);
			} else {
				return response.send(result);

			}
		})
	})


	// Activity

	app.post("/signup", (request, response)=>{
		let inputuser = request.body

			console.log(inputuser.password)
			if (inputuser.password === undefined) {
				return response.send("Please input a password")
			} else {
				User.findOne({username: inputuser.username}, (error, result) =>{
				console.log(result);

				if (result !== null) {
					return response.send("The username is already exsisting!");
				} else {
					let newUser = new User({
						username: inputuser.username,
						password: inputuser.password
					})

					newUser.save((saveError,saveUser) =>{
						if(saveError){
							return console.log(saveError);
						} else {
							response.send("New User has been created!")
						}
					})
				}
			})
		}
	})


app.listen(port, ()=> console.log(`Sever is running at port ${port}!`))